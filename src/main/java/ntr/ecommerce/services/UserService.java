package ntr.ecommerce.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ntr.ecommerce.model.User;
import ntr.ecommerce.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository utilisateurRepository;

    @Autowired
    BCryptPasswordEncoder bcrypt;

    /**
     *
     * @param id
     * @return
     */
    public User findById(Long id) {
        User utilisateur = null;
        for (User u : utilisateurRepository.findAll()) {
            if (u.getId() == id) {
                utilisateur = u;
                break;
            }
        }
        return utilisateur;
    }

    /**
     * Find user by email
     *
     * @param mail
     * @return
     */
    public User findByMail(String mail) {
        User utilisateur = null;
        for (User user : utilisateurRepository.findAll()) {
            if (user.getEmail().equalsIgnoreCase(mail)) {
                utilisateur = user;
                break;
            }
        }
        return utilisateur;
    }

    /**
     * Retrieve logged user
     *
     * @return
     */
    public User getLoggedUser() {
        String username = "";
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken))
            username = auth.getName();
        return findByMail(username);
    }

    /**
     * Register user
     *
     * @return
     */
    public User save() {
        User utilisateur = new User();
        utilisateur.setEmail("zafitina@gmail.com");
        utilisateur.setName("Zafitina");
        utilisateur.setFirstName("Nicolas");
        utilisateur.setPassword(bcrypt.encode(utilisateur.getPassword()));
        return utilisateurRepository.save(utilisateur);
    }
}
