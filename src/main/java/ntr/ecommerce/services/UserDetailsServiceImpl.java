package ntr.ecommerce.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import ntr.ecommerce.model.User;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserService utilisateurService;

    @Autowired
    RoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String mail) {
        // TODO Auto-generated method stub
        User user = utilisateurService.findByMail(mail);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().getName()));
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                grantedAuthorities);
    }
}
