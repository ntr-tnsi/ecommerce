package ntr.ecommerce.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ntr.ecommerce.model.Transaction;
import ntr.ecommerce.model.Produit;
import ntr.ecommerce.model.User;
import ntr.ecommerce.repository.ProduitRepository;

@Service
public class ProduitService {
    @Autowired
    private ProduitRepository produitRepository;

    @Autowired
    private UserService utilisateurService;

//    /**
//     *
//     * @return
//     */
//    public Boolean isAvailable(Produit produit) {
//        return produit.getNbLocataire() > 0;
//    }

    /**
     *
     * @return
     */
    public int countNotifications() {
        int notifs = 0;
        for (Transaction transaction : utilisateurService.getLoggedUser().getTransactions()) {
            if (transaction.getValider() == false) {
                notifs++;
            }
        }
        return notifs;
    }

    /**
     *
     * @return
     */
    public Set<Produit> findAllAvailable(User utilisateur) {
        Set<Produit> availables = new HashSet<Produit>();
        for (Produit produit : produitRepository.findAll()) {
            availables.add(produit);
        }
        return availables;
    }

    /**
     *
     * @param id
     * @return
     */
    public User proprietaire(Long id) {
        Produit produit = this.findById(id);
        return produit.getProprietaire();
    }

    /**
     *
     * @param id
     * @return
     */
    public Produit findById(Long id) {
        Produit produit = null;
        for (Produit l : produitRepository.findAll()) {
            if (l.getId() == id) {
                produit = l;
                break;
            }
        }
        return produit;
    }
}
