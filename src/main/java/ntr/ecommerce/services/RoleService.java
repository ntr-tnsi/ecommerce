package ntr.ecommerce.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ntr.ecommerce.model.Role;
import ntr.ecommerce.repository.RoleRepository;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    /**
     *
     * @return
     */
    public Set<Role> findAll() {
        return roleRepository.findAll();
    }

    /**
     *
     * @param name
     * @return
     */
    public Role findByName(String name) {
        Role role = null;
        for (Role r : roleRepository.findAll()) {
            if (r.getName().equalsIgnoreCase(name)) {
                role = r;
                break;
            }
        }
        return role;
    }
}
