package ntr.ecommerce.services;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ntr.ecommerce.model.Transaction;
import ntr.ecommerce.model.Produit;
import ntr.ecommerce.model.User;
import ntr.ecommerce.repository.TransactionRepository;

@Service
public class TransactionService {
    @Autowired
    private UserService utilisateurService;

    @Autowired
    private TransactionRepository transactionRepository;

    /**
     *
     * @param id
     * @return
     */
    public Transaction findById(Long id) {
        Transaction t = null;
        for (Transaction transaction : transactionRepository.findAll()) {
            if (transaction.getId() == id) {
                t = transaction;
                break;
            }
        }
        return t;
    }
}
