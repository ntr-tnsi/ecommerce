package ntr.ecommerce.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.sun.xml.bind.v2.model.core.ID;

import ntr.ecommerce.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findById(Long id);

    boolean existsById(ID id);

    User save(User utilisateur);

    Set<User> findAll();

    long count();

    void deleteById(Long id);

    void delete(User u);

    void deleteAll();
}
