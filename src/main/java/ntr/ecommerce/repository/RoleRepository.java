package ntr.ecommerce.repository;

import java.util.Optional;
import java.util.Set;

import ntr.ecommerce.model.Role;
import org.springframework.data.repository.CrudRepository;


public interface RoleRepository extends CrudRepository<Role, Long> {
    Optional<Role> findById(Long id);

    boolean existsById(Long id);

    Set<Role> findAll();

    long count();

    Role save(Role role);

    void deleteById(Long id);

    void delete(Role l);

    void deleteAll();
}
