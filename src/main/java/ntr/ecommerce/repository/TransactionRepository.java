package ntr.ecommerce.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import ntr.ecommerce.model.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
    Optional<Transaction> findById(Long id);

    boolean existsById(Long id);

    Set<Transaction> findAll();

    long count();

    void deleteById(Long id);

    void delete(Transaction l);

    void deleteAll();
}
