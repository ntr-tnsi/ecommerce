package ntr.ecommerce.controller;

import java.io.IOException;
import java.util.Date;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.net.HttpURLConnection;
import java.net.URL;


import ntr.ecommerce.model.Transaction;
import ntr.ecommerce.model.Produit;
import ntr.ecommerce.model.User;
import ntr.ecommerce.repository.TransactionRepository;
import ntr.ecommerce.services.TransactionService;
import ntr.ecommerce.services.ProduitService;
import ntr.ecommerce.services.UserService;

@CrossOrigin(origins = "*")
@Controller
public class TransactionController {
    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    ProduitService produitService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    UserService userService;

    private final String url = "/transaction";

    /**
     *
     * @return
     */
    @RequestMapping(value = url + "s", method = RequestMethod.GET)
    public ModelAndView findAll() {
        ModelAndView mav = new ModelAndView("transactions");
        User utilisateur = userService.getLoggedUser();
        System.out.println(utilisateur.getTransactions());

        mav.addObject("transactions", utilisateur.getTransactions());
        mav.addObject("utilisateur", userService.getLoggedUser());
        mav.addObject("notifications", produitService.countNotifications());
        return mav;
    }

    /**
     *
     * @return
     * @throws IOException
     */
    @RequestMapping(value = url + ".store/{id}", method = RequestMethod.POST)
    public String save( @PathVariable Long id) throws IOException {
        Produit produit = produitService.findById(id);
        User utilisateur = userService.getLoggedUser();
        Transaction transaction = new Transaction();
        transaction.setValider(false);
        transaction.setProduit(produit);
        transaction.setAcheteur(utilisateur);
        transactionRepository.save(transaction);
        produit.getTransactions().add(transaction);
        utilisateur.getTransactions().add(transaction);

        // Envoi requête pour débiter le compte de l'utilisateur
        long acheteur = utilisateur.getAccountNumber();
        long amount = produit.getPrix().longValue();
        long vendeur = produit.getProprietaire().getAccountNumber();

        // Débiter l'acheteur

        try {
            String soapBody = "<soapenv:Envelope \n" +
                    "   xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" \n" +
                    "   xmlns:ws=\"http://controller.application.com/\">\n" +
                    " <soapenv:Header/>\n" +
                    " <soapenv:Body>\n" +
                    "    <ws:credit>\n" +
                    "       <arg0>"+ amount +"</arg0>\n" +
                    "       <arg1>"+ acheteur +"</arg1>\n" +
                    "       <arg2>"+ vendeur +"</arg2>\n" +
                    "    </ws:credit>\n" +
                    " </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

             URL url = new URL("http://localhost:8080/ntr-widfly/operation");
             HttpURLConnection connection = (HttpURLConnection) url.openConnection();

             // Set timeout as per needs
             connection.setConnectTimeout(20000);
             connection.setReadTimeout(20000);

             // Set DoOutput to true if you want to use URLConnection for output.
             // Default is false
             connection.setDoOutput(true);

             connection.setUseCaches(true);
             connection.setRequestMethod("POST");

             // Set Headers
             connection.setRequestProperty("Accept", "application/xml");
             connection.setRequestProperty("Content-Type", "text/xml");
             connection.setRequestProperty("Data-Type", "xml");

             // Write XML
             OutputStream outputStream = connection.getOutputStream();

             byte[] b = soapBody.getBytes("UTF-8");
             outputStream.write(b);
             outputStream.flush();
             outputStream.close();

             // Read XML
             InputStream inputStream = connection.getInputStream();
             byte[] res = new byte[2048];
             int i = 0;
             StringBuilder response = new StringBuilder();
            while ((i = inputStream.read(res)) != -1) {
            response.append(new String(res, 0, i));
        }
        inputStream.close();

        System.out.println("SOAP Credit WORK");
        System.out.println("Reponse = " + response.toString());

        }catch (IOException e) {
            e.printStackTrace();
        }

        // Debit le vendeur

        try{
            String soapBody = "<soapenv:Envelope \n" +
                    "   xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" \n" +
                    "   xmlns:ws=\"http://controller.application.com/\">\n" +
                    " <soapenv:Header/>\n" +
                    " <soapenv:Body>\n" +
                    "    <ws:debit>\n" +
                    "       <arg0>"+ amount +"</arg0>\n" +
                    "       <arg1>"+ acheteur +"</arg1>\n" +
                    "       <arg2>"+ vendeur +"</arg2>\n" +
                    "    </ws:debit>\n" +
                    " </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            URL url = new URL("http://localhost:8080/ntr-widfly/operation");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Set timeout as per needs
            connection.setConnectTimeout(20000);
            connection.setReadTimeout(20000);

            // Set DoOutput to true if you want to use URLConnection for output.
            // Default is false
            connection.setDoOutput(true);

            connection.setUseCaches(true);
            connection.setRequestMethod("POST");

            // Set Headers
            connection.setRequestProperty("Accept", "application/xml");
            connection.setRequestProperty("Content-Type", "text/xml");
            connection.setRequestProperty("Data-Type", "xml");

            // Write XML
            OutputStream outputStream = connection.getOutputStream();

            byte[] b = soapBody.getBytes("UTF-8");
            outputStream.write(b);
            outputStream.flush();
            outputStream.close();

            // Read XML
            InputStream inputStream = connection.getInputStream();
            byte[] res = new byte[2048];
            int i = 0;
            StringBuilder response = new StringBuilder();
            while ((i = inputStream.read(res)) != -1) {
                response.append(new String(res, 0, i));
            }
            inputStream.close();

            System.out.println("SOAP Debit WORK");
            System.out.println("Reponse = " + response.toString());


        }catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/home";
    }


    /**
     *
     * @return
     */
    @RequestMapping(value = url + ".validate/{id}", method = RequestMethod.POST)
    public String validate(@PathVariable Long id) {
        Transaction transaction = transactionService.findById(id);
        transaction.setValider(true);
        transactionRepository.save(transaction);
        return "redirect:/produits";
    }

    /**
     *
     * @param id
     * @return
     */
    @RequestMapping(value = url + ".delete/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable Long id) {
        Transaction location = transactionService.findById(id);
        if (location.getValider() == true) {
        }
        transactionRepository.deleteById(id);
        return "redirect:/produits";
    }


}
