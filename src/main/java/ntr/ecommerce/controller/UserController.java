package ntr.ecommerce.controller;

import java.security.Principal;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import ntr.ecommerce.model.User;
import ntr.ecommerce.repository.UserRepository;
import ntr.ecommerce.services.RoleService;
import ntr.ecommerce.services.UserService;

@CrossOrigin(origins = "*")
@Controller
public class UserController {
    @Autowired
    UserRepository utilisateurRepository;

    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Autowired
    private UserService utilisateurService;

    @Autowired
    private RoleService roleService;

    private final String url = "/user";

    /**
     * Retrieve all users
     *
     * @return
     */
    @RequestMapping(value = url + "s", method = RequestMethod.GET)
    @ResponseBody
    public Set<User> findAll() {
        return utilisateurRepository.findAll();
    }


    /**
     *
     * @return
     */
    @RequestMapping(value = url + ".create")
    public ModelAndView create() {
        ModelAndView mav = new ModelAndView("inscription");
        mav.addObject("utilisateur", new User());
        mav.addObject("roles", roleService.findAll());
        return mav;
    }

    @RequestMapping(value = url + ".store", method = RequestMethod.POST)
    public String save(@ModelAttribute @Valid User utilisateur, BindingResult result) {
        if (result.hasErrors()) {
            System.err.println(result);
            return "redirect:/" + url + ".create";
        }
        utilisateur.setPassword(bcrypt.encode(utilisateur.getPassword()));
        utilisateurRepository.save(utilisateur);
        return "redirect:/login";
    }

//    @RequestMapping(value = url + ".signal/{id}", method = RequestMethod.POST)
//    public String signal(@PathVariable Long id) {
//        User utilisateur = utilisateurService.findById(id);
//        utilisateurService.getLoggedUser().getSignals().add(utilisateur);
//        return "redirect:/logements";
//    }

    /**
     * Retrieve logged user
     *
     * @return
     */
    @RequestMapping(value = "/loggedUser", method = RequestMethod.GET)
    @ResponseBody
    public User getLoggedUser(Principal principal) {
        return utilisateurService.getLoggedUser();
    }

}
