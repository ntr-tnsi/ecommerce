package ntr.ecommerce.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ntr.ecommerce.model.Role;
import ntr.ecommerce.model.User;
import ntr.ecommerce.repository.RoleRepository;
import ntr.ecommerce.repository.UserRepository;
import ntr.ecommerce.services.ProduitService;
import ntr.ecommerce.services.RoleService;
import ntr.ecommerce.services.UserService;

@Controller
public class MainController {
    @Autowired
    private UserService utilisateurService;

    @Autowired
    private UserRepository utilisateurRepository;

    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    ProduitService produitService;

    /**
     * Retrieve login page
     *
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView loginPage() {
        ModelAndView model = new ModelAndView("loginPage");
        return model;
    }

    /**
     *
     *
     * @return
     */
    @RequestMapping(value = { "/home", "/" }, method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView mav = new ModelAndView("homePage");
        if (utilisateurService.getLoggedUser() != null) {
            mav.addObject("utilisateur", utilisateurService.getLoggedUser());
            mav.addObject("notifications", produitService.countNotifications());
        }
        if (produitService.findAllAvailable(utilisateurService.getLoggedUser()) != null) {
            mav.addObject("produits", produitService.findAllAvailable(utilisateurService.getLoggedUser()));
        }

        return mav;
    }
}
