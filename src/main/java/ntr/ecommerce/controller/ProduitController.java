package ntr.ecommerce.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ntr.ecommerce.model.Transaction;
import ntr.ecommerce.model.Produit;
import ntr.ecommerce.model.TypeProduit;
import ntr.ecommerce.model.User;
import ntr.ecommerce.repository.TransactionRepository;
import ntr.ecommerce.repository.ProduitRepository;
import ntr.ecommerce.services.ProduitService;
import ntr.ecommerce.services.UserService;

@CrossOrigin(origins = "*")
@Controller
public class ProduitController {
    @Autowired
    private ProduitService produitService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProduitRepository produitRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    private final String url = "/produit";

    /**
     *
     * @return
     */
    @RequestMapping(value = url + "s", method = RequestMethod.GET)
    public ModelAndView findAll() {
        ModelAndView mav = new ModelAndView("produits");
        User utilisateur = userService.getLoggedUser();

        mav.addObject("produits", utilisateur.getProduits());
        mav.addObject("utilisateur", userService.getLoggedUser());
        mav.addObject("notifications", produitService.countNotifications());
        return mav;
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = url + ".create", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView mav = new ModelAndView("produit");
        mav.addObject("produit", new Produit());
        return mav;
    }

    /**
     *
     * @param id
     * @return
     */
    @RequestMapping(value = url + ".delete/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable Long id) {
        Produit produit = produitService.findById(id);
        for (Transaction transaction : produit.getTransactions()) {
            transactionRepository.delete(transaction);
        }
        produitRepository.deleteById(id);
        return "redirect:" + url + "s";
    }

    @RequestMapping(value = url
            + ".store/{prix}/{type}", method = RequestMethod.POST)
    public String save( @PathVariable Double prix,@PathVariable String type) {
        Produit produit = new Produit();
        produit.setPrix(prix);
        produit.setType(type.equalsIgnoreCase("vetements") ? TypeProduit.VETEMENTS : TypeProduit.CHAUSSURES);
        produit.setProprietaire(userService.getLoggedUser());
        produitRepository.save(produit);
        return "redirect:" + url + "s";
    }
}
